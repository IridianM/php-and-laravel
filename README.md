# ESTE ES UN BLOG PERSONAL EN LARAVEL CON SU ADMINISTRACIÓN EDITAR, ELIMINAR, ACTUALIZAR, GUARDAR #


### Todos los paquetes slug que están relacionados con eloquent. ###


* Instalamos un nuevo proyecto:
`
composer create-project --prefer-dist laravel/laravel basic
`
* ponemos auth:
`
php artisan make:auth
`
* agregamos un nuevo controlador, factory y modelo:
`
php artisan make:model Post -mfc
`
* Eloquent-sluggable Para descargar por consola la siguiente librería que vienen de composer, hacemos esto en la terminal de comandos:

`
composer require cvievbrock/eloquent-sluggable
`