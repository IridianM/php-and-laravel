<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route folder views: resources/views.

// utilizandolo desde el controlador. // this is an example with a normal route
// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/holair', function () {
//     return 'Hola Iridian';
// });

//An example with un array asociativo.
// Route::get('/test', function () {
//  return [
//      'title' => 'Curso Laravel',
//      'nombre' => 'Iridian'
//  ];
//  });

//An example with un array asociativo and a view.
// Route::get('/test', function () {
//     return view ( 'test',
//     [
//         'title' => 'Curso Laravel by Iridian'
//     ]);
// });

//utilizando desde el closure y no desde el controlador
//index, le decimos en nombre de la ruta y el nombre del controlador que se va a usar.
Route::get('/', 'HomeController@index');

Route::get('/dashboard', 'DashboardController@index');

Route::get('/examples', 'exampleController@index');

Route::resource('/expense_reports', 'ExpenseReportController');
Route::get('/expense_reports/{id}/confirmDelete', 'ExpenseReportController@confirmDelete');
Route::get('/expense_reports/{expense_report}/expenses/create', 'ExpenseController@create');
Route::post('/expense_reports/{expense_report}/expenses', 'ExpenseController@store');
