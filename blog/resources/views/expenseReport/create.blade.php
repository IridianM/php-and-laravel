<!--Route where is ubicated the template-->
@extends('layouts.base')
<!--The section goes in the layout base page-->
@section('content')
    <div class="row">
        <div class="col">
            <h1>Reports</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="btn btn-secondary" href="/expense_reports">Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <!--Laravel incluye slgunos errores dentro de una variable llamada errors-->
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="/expense_reports" method="POST">
               <!--Verificación de seguridad ante estos ataques-->
                @csrf
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Type a title" value="{{ old('title') }}">
                </div>
                <button class="btn btn-primary" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection