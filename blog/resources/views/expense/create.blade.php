<!--Route where is ubicated the template-->
@extends('layouts.base')
<!--The section goes in the layout base page-->
@section('content')
    <div class="row">
        <div class="col">
            <h1>New Expense</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="btn btn-secondary" href="/expense_reports/{{ $report->id }}">Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <!--Laravel incluye slgunos errores dentro de una variable llamada errors-->
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <form action="/expense_reports/{{ $report->id }}/expenses" method="POST">
               <!--Verificación de seguridad ante estos ataques-->
                @csrf
                <div class="form-group">
                    <label for="title">Description:</label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Type a description" value="{{ old('description') }}">
                </div>
                <div class="form-group">
                    <label for="title">amount:</label>
                    <input type="text" class="form-control" id="amount" name="amount" placeholder="Type a amount" value="{{ old('amount') }}">
                </div>
                <button class="btn btn-primary" type="submit">
                    Submit
                </button>
            </form>
        </div>
    </div>
@endsection