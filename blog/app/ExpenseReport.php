<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseReport extends Model
{
   //Para decir que estan relacionados.
   public function expenses () {
    return $this->hasMany(Expense::class);
    }
}
