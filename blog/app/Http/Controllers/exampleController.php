<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class exampleController extends Controller
{
    //
    public function index (Request $request) {
        //Vista/Página HTML que va a ser usada para este controlador.
        return view('example');
    }
}
