<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //laravel utiliza inyección/ injection de dependencias
    //cuando detecta una variable de tipo request sabe que lo que 
    //debe inyectar es el request que esta accediendo a nuestra acción. 
    
    public function index (Request $request) {
        //var_dump y die;
        //dd($request->query('title', 'valor default'));
        return view ( 'test',
            [
                'title' => $request->query('title', 'valor default')
            ]);
    }
}
