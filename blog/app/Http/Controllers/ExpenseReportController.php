<?php

namespace App\Http\Controllers;
use App\ExpenseReport;
use Illuminate\Http\Request;

class ExpenseReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //the view
        return view ('expenseReport.index', 
            [   //the db
                'expensesReports' => ExpenseReport::all()
            ]
        );
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   //The HTML view.
        return view ('expenseReport.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validations
        $validData = $request->validate([
            //Aqui lo validamos y le decimos de cuantos caracteres minimo se deben escribir para dejar guardar los datos.
            'title' => 'required|min:3'
        ]);
        //Store in BD
        $report = new ExpenseReport();
        //un title viene de la bd Y EL GET LO TRAE DESDE EL TEMPLATE
        //$report->title = $request->get('title');
        $report->title = $validData['title'];
        $report->save();

        //cuándo el registro se guarde nos va a mandar a esta siguiente pantalla.
        return redirect('/expense_reports');
    }

    /**
     * Display the specified resource.
     *@param  int  ExpenseReport $expenseReport
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Whith modelBinding it is like this:
    public function show(ExpenseReport $expenseReport)
    {
        return view('expenseReport.show', 
            [
                'report' => $expenseReport
            ]
        );
    }

    //Without modelBinding it will be like this:
    // public function show($id)
    // {
    //     $report = ExpenseReport::findOrFail($id);
    //     return view('expenseReport.show', 
    //     [
    //         'report' => $report
    //     ]
    // );
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = ExpenseReport::findOrFail($id);
        return view('expenseReport.edit', 
        [
            'report' => $report
        ]
    );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = ExpenseReport::find($id);
        $report->title = $request->get('title');
        $report->save();
        //Redirect to this page.
        return redirect('/expense_reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = ExpenseReport::find($id);
        $report->delete();

        return redirect('/expense_reports');
    }
    //DELETE
    public function confirmDelete($id) {
        //dd('HI');
        $report = ExpenseReport::find($id);
        return view ('expenseReport.confirmDelete',
            [
                'report' => $report
            ]
        );
    }
}
