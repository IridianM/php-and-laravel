<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //Existe una relación con un reporte.
    public function expenseReport () {
        return $this->belongsTo(ExpenseReport::class);
    }
}
